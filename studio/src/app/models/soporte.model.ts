export class TiposSoporte {
    nombre:string;
    url:string;
    descripcion:string;

    constructor(n:string, u:string, d:string) {
        this.nombre = n;
        this.url = u;
        this.descripcion = d;
    }
}