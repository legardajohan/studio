import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaSoporteComponent } from './lista-soporte.component';

describe('ListaSoporteComponent', () => {
  let component: ListaSoporteComponent;
  let fixture: ComponentFixture<ListaSoporteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaSoporteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaSoporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
