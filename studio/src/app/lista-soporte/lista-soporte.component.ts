import { Component, OnInit } from '@angular/core';
import { TiposSoporte } from './../models/soporte.model';

@Component({
  selector: 'app-lista-soporte',
  templateUrl: './lista-soporte.component.html',
  styleUrls: ['./lista-soporte.component.css']
})
export class ListaSoporteComponent implements OnInit {
  soportes: TiposSoporte[];
  constructor() {
    this.soportes = [];
   }

  ngOnInit(): void {
  }
  guardar(nombre:string, url:string, descripcion:string):boolean {
    this.soportes.push(new TiposSoporte(nombre, url, descripcion));
    return false;
  }

}
