import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaludoComponent } from './saludo/saludo.component';
import { SoporteComponent } from './soporte/soporte.component';
import { ListaSoporteComponent } from './lista-soporte/lista-soporte.component';

@NgModule({
  declarations: [
    AppComponent,
    SaludoComponent,
    SoporteComponent,
    ListaSoporteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
