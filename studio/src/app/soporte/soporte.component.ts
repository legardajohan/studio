import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { TiposSoporte } from './../models/soporte.model';

@Component({
  selector: 'app-soporte',
  templateUrl: './soporte.component.html',
  styleUrls: ['./soporte.component.css']
})
export class SoporteComponent implements OnInit {
  @Input() soporte: TiposSoporte;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() {}

  ngOnInit(): void {
  }

}
